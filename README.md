## preambule

Internal network infrastructure and at `<rgoncalves.se>`.

What it is and what it's not :

- **it is** more code, scripting and configuration files that I write and manage during my spare time.
- although, **it is NOT** a finished solution, product or `<commercial adjective here>` ready for production.
- **it is** another solution among thousand of other softwares for managing small and medium sized infrastructures.
However, that allows me to fully understand what's going under the hood when programming or using my softwares.
That's why I think I invested my time really well by doing everything from scratch, but it did cost an harder progression curve.

## repositories

### [ansible](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-ansible)

Ansible repository for managing all instances and devices at `<rgoncalves.se>` 

### [docs](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-docs)

Internal documentation, mostly used as a proof of concept and for keeping tracks of all changes.

### [generation](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-generation) (no longer maintained)

Python module for generating all kind of templates and variables, according to one master file (ansible hosts, diagram vizualization, bash automation, ssh management, ...).

> Moved to `infrastructure/infrastructure-scripts` and `infrastructure/infrastructure-visualizer`

### [scripts](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-scripts)

Shell scripts for infrastructure management, such as :

- git scripts
- ipmi scripts
- wireguard management

### [visualizer](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-visualizer)

Python web application for previewing internal infrastructure and connections between devices.
